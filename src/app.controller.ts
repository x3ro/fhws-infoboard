import { Controller, Get, Param, Redirect, ParamData } from '@nestjs/common';
import { AppService } from './app.service';

@Controller()
export class AppController {
    constructor(private readonly appService: AppService) { }

    @Get()
    async getHtml(): Promise<string> {
        const html = await this.appService.getInfoboardHtml();
        return this.appService.injectJavascript(html)
    }

    @Get(':path([^/]*)')
    @Redirect('')
    getStatic(@Param('path') path: any) {
        return {
            url: `https://infoboard.fhws.de/${path}`,
            statusCode: 308,
        }
    }
}
