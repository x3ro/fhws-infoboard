import { Injectable } from '@nestjs/common';
import axios from "axios";
import * as detectCharacterEncoding from "detect-character-encoding"
import * as iconv from 'iconv-lite'
import { JSDOM } from 'jsdom'

@Injectable()
export class AppService {
    async getInfoboardHtml(): Promise<string> {
        const response = await axios.get("https://infoboard.fhws.de/", {responseType: "arraybuffer"})
        const buffer = response.data

        const { encoding, confidence } = detectCharacterEncoding(buffer)
        // TODO: Test if encoding exists (iconv.encodingExists(encoding))
        // console.log({encoding, confidence});

        const decoded = iconv.decode(buffer, encoding)

        return decoded.toString()
    }

    injectJavascript(html: string): string {
        const [ originalDoctype ] = html.match(/^<!DOCTYPE[^>]*?>/)
        const dom = new JSDOM(html)

        const document = dom.window.document
        const body = document.querySelector('body')
        const injectScript = document.createElement('script')

        injectScript.setAttribute('src', '/_/assets/inject.js')
        body.appendChild(injectScript)

        return dom
            .serialize()
            .replace(/^<!DOCTYPE[^>]*?>/, originalDoctype)
    }
}
