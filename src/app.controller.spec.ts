import { Test, TestingModule } from '@nestjs/testing';
import { AppController } from './app.controller';
import { AppService } from './app.service';

describe('AppController', () => {
    let appController: AppController;

    beforeEach(async () => {
        const app: TestingModule = await Test.createTestingModule({
            controllers: [AppController],
            providers: [AppService],
        }).compile();

        appController = app.get<AppController>(AppController);
    });

    describe('root', () => {
        it('should return FHWS Infoboard HTML', async () => {
            expect(await appController.getHtml()).toContain('<title>FHWS Laufende Lehrveranstaltungen</title>');
        });

        it('should redirect static content', () => {
            expect(
                appController.getStatic('imageName.png')
            ).toMatchObject({
                "url": "https://infoboard.fhws.de/imageName.png",
                "statusCode": 308,
            })
        })

        it('should redirect static content for any given path', () => {
            expect(
                appController.getStatic('path/to/image.png')
            ).toMatchObject({
                "url": "https://infoboard.fhws.de/path/to/image.png",
                "statusCode": 308,
            })
        })
    });
});
