import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import * as helmet from 'helmet';
import { NestExpressApplication } from "@nestjs/platform-express";
import path = require('path');
import { Logger } from "nestjs-pino";

const PORT = 8042

async function bootstrap() {
    const isDevelop = process.env['APP_ENV'] === 'development'

    const app = await NestFactory.create<NestExpressApplication>(AppModule, {
        logger: (isDevelop ? undefined : console),
        cors: true,
    });
    app.useLogger(app.get(Logger))
    app.useStaticAssets(path.join(__dirname, '..', 'public'), {
        prefix: '/_/assets',
    });
    app.use(helmet())

    await app.listen(PORT);
}
bootstrap();
